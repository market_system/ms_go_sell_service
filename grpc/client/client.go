package client

import (
	"gitlab.com/market_system/ms_go_sell_service/config"
	"gitlab.com/market_system/ms_go_sell_service/genproto/load_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	Remainder() load_service.RemainderServiceClient
	Product() load_service.ProductServiceClient
}

type grpcClients struct {
	remainder load_service.RemainderServiceClient
	product   load_service.ProductServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {

	connLoadService, err := grpc.Dial(
		cfg.LOADServiceHost+cfg.LOADGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		remainder: load_service.NewRemainderServiceClient(connLoadService),
		product:   load_service.NewProductServiceClient(connLoadService),
	}, nil
}

func (g *grpcClients) Remainder() load_service.RemainderServiceClient {
	return g.remainder
}

func (g *grpcClients) Product() load_service.ProductServiceClient {
	return g.product
}
