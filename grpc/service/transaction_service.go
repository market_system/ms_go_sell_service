package service

import (
	"context"

	"gitlab.com/market_system/ms_go_sell_service/config"
	"gitlab.com/market_system/ms_go_sell_service/genproto/sell_service"
	"gitlab.com/market_system/ms_go_sell_service/grpc/client"
	"gitlab.com/market_system/ms_go_sell_service/models"
	"gitlab.com/market_system/ms_go_sell_service/pkg/logger"
	"gitlab.com/market_system/ms_go_sell_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type TransactionService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*sell_service.UnimplementedTransactionServiceServer
}

func NewTransactionService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *TransactionService {
	return &TransactionService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *TransactionService) Create(ctx context.Context, req *sell_service.CreateTransaction) (resp *sell_service.Transaction, err error) {

	u.log.Info("-----------CreateTransaction----------", logger.Any("req", req))

	id, err := u.strg.Transaction().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateTransaction ->Transaction->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Transaction().GetByPKey(ctx, id)
	if err != nil {
		u.log.Error("GetByIdTransaction->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *TransactionService) GetByID(ctx context.Context, req *sell_service.TransactionPrimaryKey) (resp *sell_service.Transaction, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Transaction().GetByPKey(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Transaction->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *TransactionService) GetList(ctx context.Context, req *sell_service.GetListTransactionRequest) (*sell_service.GetListTransactionResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Transaction().GetAll(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Transaction->GetAll", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *TransactionService) Update(ctx context.Context, req *sell_service.UpdateTransaction) (resp *sell_service.Transaction, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Transaction().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Transaction->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Transaction().GetByPKey(ctx, &sell_service.TransactionPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Transaction->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (i *TransactionService) UpdatePatch(ctx context.Context, req *sell_service.UpdatePatchTransaction) (resp *sell_service.Transaction, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Transaction().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchTransaction--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Transaction().GetByPKey(ctx, &sell_service.TransactionPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->Transaction->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *TransactionService) Delete(ctx context.Context, req *sell_service.TransactionPrimaryKey) (resp *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	resp, err = u.strg.Transaction().Delete(ctx, &sell_service.TransactionPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Transaction->Delete", logger.Error(err))
		return
	}

	return
}
