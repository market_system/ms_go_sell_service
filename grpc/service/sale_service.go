package service

import (
	"context"
	"errors"
	"strings"

	"gitlab.com/market_system/ms_go_sell_service/config"
	"gitlab.com/market_system/ms_go_sell_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_sell_service/genproto/sell_service"
	"gitlab.com/market_system/ms_go_sell_service/grpc/client"
	"gitlab.com/market_system/ms_go_sell_service/models"
	"gitlab.com/market_system/ms_go_sell_service/pkg/logger"
	"gitlab.com/market_system/ms_go_sell_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

var (
	PROCESS  = "in_process"
	FINISHED = "finished"
)

type SalesService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*sell_service.UnimplementedSalesServiceServer
	*load_service.UnimplementedRemainderServiceServer
}

func NewSalesService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *SalesService {
	return &SalesService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *SalesService) Create(ctx context.Context, req *sell_service.CreateSales) (resp *sell_service.Sales, err error) {

	u.log.Info("-----------CreateSales----------", logger.Any("req", req))

	_, err = u.strg.Shift().GetByPKey(ctx, &sell_service.ShiftPrimaryKey{Id: req.ShiftId})
	if err != nil {
		u.log.Error("Create->Sales->SHIFT->GetByID")
		return nil, errors.New("There is no any shift account!")
	}

	id, err := u.strg.Sales().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateSales ->Sales->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Sales().GetByPKey(ctx, id)
	if err != nil {
		u.log.Error("GetByIdSales->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *SalesService) GetByID(ctx context.Context, req *sell_service.SalesPrimaryKey) (resp *sell_service.Sales, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Sales().GetByPKey(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Sales->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *SalesService) GetList(ctx context.Context, req *sell_service.GetListSalesRequest) (*sell_service.GetListSalesResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Sales().GetAll(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Sales->GetAll", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *SalesService) Update(ctx context.Context, req *sell_service.UpdateSales) (*sell_service.Sales, error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err := u.strg.Sales().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Sales->Update", logger.Error(err))
		return nil, err
	}

	s_update, err := u.strg.Sales().GetByPKey(ctx, &sell_service.SalesPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Sales->GetByPKey", logger.Error(err))
		return nil, err
	}

	if strings.ToLower(s_update.Status) == strings.ToLower(FINISHED) {
		payment, err := u.strg.Payment().GetByPKey(ctx, &sell_service.PaymentPrimaryKey{Id: s_update.PaymentId})
		if err != nil {
			u.log.Error("Update ->Sales->GetByPKey ->PAYMENT", logger.Error(err))
			return nil, err
		}

		shift, err := u.strg.Shift().GetByPKey(ctx, &sell_service.ShiftPrimaryKey{Id: s_update.ShiftId})
		if err != nil {
			u.log.Error("Update ->Sales->GetByPKey ->SHIFT", logger.Error(err))
			return nil, err
		}

		transaction, err := u.strg.Transaction().GetByPKey(ctx, &sell_service.TransactionPrimaryKey{Id: shift.TransactionId})
		if err != nil {
			u.log.Error("Update ->Sales->GetByPKey ->TRANSACTION", logger.Error(err))
			return nil, err
		}
		_, err = u.strg.Transaction().Update(ctx, &sell_service.UpdateTransaction{
			Id:         transaction.Id,
			Cash:       transaction.Cash + payment.Cash + payment.CurrencyCount*payment.ExchangeSum,
			Uzcard:     transaction.Uzcard + payment.Uzcard,
			Payme:      transaction.Payme + payment.Payme,
			Click:      transaction.Click + payment.Click,
			Humo:       transaction.Humo + payment.Humo,
			Apelsin:    transaction.Apelsin + payment.Apelsin,
			TotalPrice: transaction.TotalPrice + payment.TotalPrice,
		})
		if err != nil {
			u.log.Error("Update ->Sales->Update ->TRANSACTION", logger.Error(err))
			return nil, err
		}

		prods, err := u.strg.SalesProduct().GetAll(ctx, &sell_service.GetListSalesProductRequest{SaleId: s_update.Id})
		if err != nil {
			u.log.Error("Update -> Sales ->Update->SALES PRODUCT ->GetAll")
		}

		for i := 0; i < int(prods.Count); i++ {
			rem, err := u.service.Remainder().GetByID(ctx, &load_service.RemainderPrimaryKey{Id: prods.SalesProducts[i].RemainderId})
			if err != nil {
				u.log.Error("Update->Sales->Update->REMAINDER ->GetByID")
				return nil, err
			}

			_, err = u.service.Remainder().Update(ctx, &load_service.UpdateRemainder{
				Id:         rem.Id,
				BranchId:   rem.BranchId,
				CategoryId: rem.CategoryId,
				ProductId:  rem.ProductId,
				BrandId:    rem.BrandId,
				Name:       rem.BrandId,
				Count:      rem.Count - prods.SalesProducts[i].Count,
				Barcode:    rem.Barcode,
			})
			if err != nil {
				u.log.Error("Update->Sales->Update->REMAINDER-> Update")
				return nil, err
			}
			shiftt, err := u.strg.Shift().GetByPKey(ctx, &sell_service.ShiftPrimaryKey{Id: s_update.ShiftId})
			if err != nil {
				u.log.Error("Update ->Sales->GetByPKey ->SHIFT", logger.Error(err))
				return nil, err
			}

			_, err = u.strg.Shift().Update(ctx, &sell_service.UpdateShift{
				Id:            shiftt.Id,
				BranchId:      shiftt.BranchId,
				StaffId:       shiftt.StaffId,
				MarketId:      shiftt.MarketId,
				TransactionId: shiftt.TransactionId,
				Status:        shiftt.Status,
				Count:         shiftt.Count + prods.SalesProducts[i].Count,
				Price:         shiftt.Price,
			})
			if err != nil {
				u.log.Error("Update ->Sales->GetByPKey ->SHIFT", logger.Error(err))
				return nil, err
			}

		}

		shiftt, err := u.strg.Shift().GetByPKey(ctx, &sell_service.ShiftPrimaryKey{Id: s_update.ShiftId})
		if err != nil {
			u.log.Error("Update ->Sales->GetByPKey ->SHIFT", logger.Error(err))
			return nil, err
		}

		_, err = u.strg.Shift().Update(ctx, &sell_service.UpdateShift{
			Id:            shiftt.Id,
			BranchId:      shiftt.BranchId,
			StaffId:       shiftt.StaffId,
			MarketId:      shiftt.MarketId,
			TransactionId: shiftt.TransactionId,
			Status:        shiftt.Status,
			Count:         shiftt.Count,
			Price:         payment.TotalPrice + shift.Price,
		})
		if err != nil {
			u.log.Error("Update ->Sales->GetByPKey ->SHIFT", logger.Error(err))
			return nil, err
		}

	}

	resp, err := u.strg.Sales().GetByPKey(ctx, &sell_service.SalesPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Sales->GetByPKey", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (i *SalesService) UpdatePatch(ctx context.Context, req *sell_service.UpdatePatchSales) (resp *sell_service.Sales, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Sales().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchSales--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Sales().GetByPKey(ctx, &sell_service.SalesPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->Sales->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *SalesService) Delete(ctx context.Context, req *sell_service.SalesPrimaryKey) (resp *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	resp, err = u.strg.Sales().Delete(ctx, &sell_service.SalesPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Sales->Delete", logger.Error(err))
		return
	}

	return
}
