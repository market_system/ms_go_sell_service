package service

import (
	"context"

	"gitlab.com/market_system/ms_go_sell_service/config"
	"gitlab.com/market_system/ms_go_sell_service/genproto/sell_service"
	"gitlab.com/market_system/ms_go_sell_service/grpc/client"
	"gitlab.com/market_system/ms_go_sell_service/models"
	"gitlab.com/market_system/ms_go_sell_service/pkg/logger"
	"gitlab.com/market_system/ms_go_sell_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type PaymentService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*sell_service.UnimplementedPaymentServiceServer
}

func NewPaymentService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *PaymentService {
	return &PaymentService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *PaymentService) Create(ctx context.Context, req *sell_service.CreatePayment) (resp *sell_service.Payment, err error) {

	u.log.Info("-----------CreatePayment----------", logger.Any("req", req))

	id, err := u.strg.Payment().Create(ctx, req)
	if err != nil {
		u.log.Error("CreatePayment ->Payment->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Payment().GetByPKey(ctx, id)
	if err != nil {
		u.log.Error("GetByIdPayment->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *PaymentService) GetByID(ctx context.Context, req *sell_service.PaymentPrimaryKey) (resp *sell_service.Payment, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Payment().GetByPKey(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Payment->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *PaymentService) GetList(ctx context.Context, req *sell_service.GetListPaymentRequest) (*sell_service.GetListPaymentResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Payment().GetAll(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Payment->GetAll", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *PaymentService) Update(ctx context.Context, req *sell_service.UpdatePayment) (resp *sell_service.Payment, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Payment().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Payment->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.Payment().GetByPKey(ctx, &sell_service.PaymentPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Payment->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (i *PaymentService) UpdatePatch(ctx context.Context, req *sell_service.UpdatePatchPayment) (resp *sell_service.Payment, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Payment().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchPayment--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Payment().GetByPKey(ctx, &sell_service.PaymentPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->Payment->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *PaymentService) Delete(ctx context.Context, req *sell_service.PaymentPrimaryKey) (resp *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	resp, err = u.strg.Payment().Delete(ctx, &sell_service.PaymentPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Payment->Delete", logger.Error(err))
		return
	}

	return
}
