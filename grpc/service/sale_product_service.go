package service

import (
	"context"
	"fmt"

	"gitlab.com/market_system/ms_go_sell_service/config"
	"gitlab.com/market_system/ms_go_sell_service/genproto/load_service"
	"gitlab.com/market_system/ms_go_sell_service/genproto/sell_service"
	"gitlab.com/market_system/ms_go_sell_service/grpc/client"
	"gitlab.com/market_system/ms_go_sell_service/models"
	"gitlab.com/market_system/ms_go_sell_service/pkg/logger"
	"gitlab.com/market_system/ms_go_sell_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type SalesProductService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*sell_service.UnimplementedSalesProductServiceServer
	*load_service.UnimplementedProductServiceServer
	*load_service.UnimplementedRemainderServiceServer
}

func NewSalesProductService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *SalesProductService {
	return &SalesProductService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *SalesProductService) Scan(ctx context.Context, req *sell_service.ScanBarcode) (resp *sell_service.SalesProduct, err error) {

	u.log.Info("-----------SCANBARCODE----------", logger.Any("req", req))

	prod, err := u.service.Product().GetByID(ctx, &load_service.ProductPrimaryKey{Id: req.ProductId})
	if err != nil {
		u.log.Error("Scan ->SalesProduct->PRODUCT->GETBYID", logger.Error(err))
		return
	}

	rem, err := u.service.Remainder().GetByID(ctx, &load_service.RemainderPrimaryKey{Id: prod.RemainderId})
	if err != nil {
		u.log.Error("Scan ->SalesProduct->REMAINDER->GETBYID", logger.Error(err))
		return
	}

	s_prod, err := u.strg.SalesProduct().GetAll(ctx, &sell_service.GetListSalesProductRequest{
		SaleId:    req.SaleId,
		ProductId: req.ProductId,
	})
	if err != nil {
		u.log.Error("Scan ->SalesProduct->GETALL", logger.Error(err))
		return
	}
	if s_prod.Count > 0 {
		_, err = u.strg.SalesProduct().Update(ctx, &sell_service.UpdateSalesProduct{
			Id:        s_prod.SalesProducts[0].Id,
			ProductId: prod.Id,
			Count:     s_prod.SalesProducts[0].Count + 1,
		})
		if err != nil {
			u.log.Error("Scan ->SalesProduct->UPDATE", logger.Error(err))
			return
		}
		_, err = u.service.Remainder().Update(ctx, &load_service.UpdateRemainder{
			Id:         rem.Id,
			BranchId:   rem.BranchId,
			CategoryId: rem.CategoryId,
			ProductId:  rem.ProductId,
			BrandId:    rem.BrandId,
			Name:       rem.Name,
			Count:      rem.Count - 1,
			Barcode:    rem.Barcode,
		})

		if err != nil {
			u.log.Error("Scan ->REMAINDER->UPDATE", logger.Error(err))
			return
		}

		resp, err = u.strg.SalesProduct().GetByPKey(ctx, &sell_service.SalesProductPrimaryKey{Id: s_prod.SalesProducts[0].Id})
		if err != nil {
			u.log.Error("scan ->GetByIdSalesProduct->GetByPKey", logger.Error(err))
			return
		}
		return
	}

	id, err := u.strg.SalesProduct().Create(ctx, &sell_service.CreateSalesProduct{
		BrandId:     prod.BrandId,
		ProductId:   prod.Id,
		BarCode:     prod.Barcode,
		RemainderId: prod.RemainderId,
		Count:       1,
		Price:       prod.Price,
		TotalPrice:  prod.Price,
		SaleId:      req.SaleId,
	})
	if err != nil {
		u.log.Error("Scan ->SalesProduct->Create", logger.Error(err))
		return
	}

	_, err = u.service.Remainder().Update(ctx, &load_service.UpdateRemainder{
		Id:         rem.Id,
		BranchId:   rem.BranchId,
		CategoryId: rem.CategoryId,
		ProductId:  rem.ProductId,
		BrandId:    rem.BrandId,
		Name:       rem.Name,
		Count:      rem.Count - 1,
		Barcode:    rem.Barcode,
	})

	if err != nil {
		u.log.Error("Scan ->REMAINDER->UPDATE", logger.Error(err))
		return
	}

	resp, err = u.strg.SalesProduct().GetByPKey(ctx, id)
	if err != nil {
		u.log.Error("Scan ->GetByIdSalesProduct->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *SalesProductService) Create(ctx context.Context, req *sell_service.CreateSalesProduct) (resp *sell_service.SalesProduct, err error) {

	u.log.Info("-----------CreateSalesProduct----------", logger.Any("req", req))

	prod, err := u.service.Product().GetByID(ctx, &load_service.ProductPrimaryKey{Id: req.ProductId})
	if err != nil {
		u.log.Error("CreateSalesProduct ->Product->GetBYID", logger.Error(err))
		return
	}
	fmt.Println(prod)

	req.TotalPrice = req.GetCount() * prod.GetPrice()
	req.Price = prod.GetPrice()

	id, err := u.strg.SalesProduct().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateSalesProduct ->SalesProduct->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.SalesProduct().GetByPKey(ctx, id)
	if err != nil {
		u.log.Error("GetByIdSalesProduct->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *SalesProductService) GetByID(ctx context.Context, req *sell_service.SalesProductPrimaryKey) (resp *sell_service.SalesProduct, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.SalesProduct().GetByPKey(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->SalesProduct->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *SalesProductService) GetList(ctx context.Context, req *sell_service.GetListSalesProductRequest) (*sell_service.GetListSalesProductResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.SalesProduct().GetAll(ctx, req)
	if err != nil {
		u.log.Error("GetList ->SalesProduct->GetAll", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *SalesProductService) Update(ctx context.Context, req *sell_service.UpdateSalesProduct) (resp *sell_service.SalesProduct, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.SalesProduct().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->SalesProduct->Update", logger.Error(err))
		return
	}
	resp, err = u.strg.SalesProduct().GetByPKey(ctx, &sell_service.SalesProductPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->SalesProduct->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (i *SalesProductService) UpdatePatch(ctx context.Context, req *sell_service.UpdatePatchSalesProduct) (resp *sell_service.SalesProduct, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.SalesProduct().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchSalesProduct--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.SalesProduct().GetByPKey(ctx, &sell_service.SalesProductPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->SalesProduct->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *SalesProductService) Delete(ctx context.Context, req *sell_service.SalesProductPrimaryKey) (resp *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	resp, err = u.strg.SalesProduct().Delete(ctx, &sell_service.SalesProductPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->SalesProduct->Delete", logger.Error(err))
		return
	}

	return
}
