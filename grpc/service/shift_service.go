package service

import (
	"context"

	"gitlab.com/market_system/ms_go_sell_service/config"
	"gitlab.com/market_system/ms_go_sell_service/genproto/sell_service"
	"gitlab.com/market_system/ms_go_sell_service/grpc/client"
	"gitlab.com/market_system/ms_go_sell_service/models"
	"gitlab.com/market_system/ms_go_sell_service/pkg/logger"
	"gitlab.com/market_system/ms_go_sell_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

type ShiftService struct {
	cfg     config.Config
	log     logger.LoggerI
	strg    storage.StorageI
	service client.ServiceManagerI
	*sell_service.UnimplementedShiftServiceServer
}

func NewShiftService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *ShiftService {
	return &ShiftService{
		cfg:     cfg,
		strg:    strg,
		log:     log,
		service: srvc,
	}
}

func (u *ShiftService) Create(ctx context.Context, req *sell_service.CreateShift) (resp *sell_service.Shift, err error) {

	u.log.Info("-----------CreateShift----------", logger.Any("req", req))

	id, err := u.strg.Shift().Create(ctx, req)
	if err != nil {
		u.log.Error("CreateShift ->Shift->Create", logger.Error(err))
		return
	}

	resp, err = u.strg.Shift().GetByPKey(ctx, id)
	if err != nil {
		u.log.Error("GetByIdShift->GetByPKey", logger.Error(err))
		return
	}

	idT, err := u.strg.Transaction().Create(ctx, &sell_service.CreateTransaction{})
	if err != nil {
		u.log.Error("Create ->Shift ->TRANSACTION->Create")
		return
	}

	_, err = u.strg.Shift().Update(ctx, &sell_service.UpdateShift{
		Id:            id.Id,
		BranchId:      resp.BranchId,
		StaffId:       resp.StaffId,
		MarketId:      resp.MarketId,
		TransactionId: idT.Id,
		Status:        req.Status,
		Count:         resp.Count,
		Price:         resp.Price,
	})
	if err != nil {
		u.log.Error("Create ->Shift ->TRANSACTION->Create")
		return
	}

	return
}

func (u *ShiftService) GetByID(ctx context.Context, req *sell_service.ShiftPrimaryKey) (resp *sell_service.Shift, err error) {
	u.log.Info("-----------GetById----------", logger.Any("req", req))

	resp, err = u.strg.Shift().GetByPKey(ctx, req)
	if err != nil {
		u.log.Error("GetByID ->Shift->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (u *ShiftService) GetList(ctx context.Context, req *sell_service.GetListShiftRequest) (*sell_service.GetListShiftResponse, error) {
	u.log.Info("-----------GetList----------")

	resp, err := u.strg.Shift().GetAll(ctx, req)
	if err != nil {
		u.log.Error("GetList ->Shift->GetAll", logger.Error(err))
		return nil, err
	}

	return resp, nil
}

func (u *ShiftService) Update(ctx context.Context, req *sell_service.UpdateShift) (resp *sell_service.Shift, err error) {
	u.log.Info("-----------Update----------", logger.Any("req", req))

	_, err = u.strg.Shift().Update(ctx, req)

	if err != nil {
		u.log.Error("Update ->Shift->Update", logger.Error(err))
		return
	}

	if req.Status == "CLOSE SHIFT" {
		u.log.Info("SHIFT SUCCESSFULLY CLOSED!")
	}

	resp, err = u.strg.Shift().GetByPKey(ctx, &sell_service.ShiftPrimaryKey{Id: req.Id})
	if err != nil {
		u.log.Error("Update ->Shift->GetByPKey", logger.Error(err))
		return
	}

	return
}

func (i *ShiftService) UpdatePatch(ctx context.Context, req *sell_service.UpdatePatchShift) (resp *sell_service.Shift, err error) {

	i.log.Info("---UpdatePatch------>", logger.Any("req", req))

	updatePatchModel := models.UpdatePatchRequest{
		Id:     req.GetId(),
		Fields: req.GetFields().AsMap(),
	}

	rowsAffected, err := i.strg.Shift().Patch(ctx, &updatePatchModel)

	if err != nil {
		i.log.Error("!!!UpdatePatchShift--->", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	if rowsAffected <= 0 {
		return nil, status.Error(codes.InvalidArgument, "no rows were affected")
	}

	resp, err = i.strg.Shift().GetByPKey(ctx, &sell_service.ShiftPrimaryKey{Id: req.Id})
	if err != nil {
		i.log.Error("!!!GetUser->Shift->Get--->", logger.Error(err))
		return nil, status.Error(codes.NotFound, err.Error())
	}

	return resp, err
}

func (u *ShiftService) Delete(ctx context.Context, req *sell_service.ShiftPrimaryKey) (_ *emptypb.Empty, err error) {
	u.log.Info("-----------Delete----------", logger.Any("req", req))

	err = u.strg.Shift().Delete(ctx, &sell_service.ShiftPrimaryKey{Id: req.Id})

	if err != nil {
		u.log.Error("Delete ->Shift->Delete", logger.Error(err))
		return
	}

	return
}
