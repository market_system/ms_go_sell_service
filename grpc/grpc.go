package grpc

import (
	"gitlab.com/market_system/ms_go_sell_service/config"
	"gitlab.com/market_system/ms_go_sell_service/genproto/sell_service"
	"gitlab.com/market_system/ms_go_sell_service/grpc/client"
	"gitlab.com/market_system/ms_go_sell_service/grpc/service"
	"gitlab.com/market_system/ms_go_sell_service/pkg/logger"
	"gitlab.com/market_system/ms_go_sell_service/storage"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	// user_service.RegisterStaffServiceServer(grpcServer, service.NewStaffService(cfg, log, strg, srvc))
	sell_service.RegisterSalesServiceServer(grpcServer, service.NewSalesService(cfg, log, strg, srvc))
	sell_service.RegisterSalesProductServiceServer(grpcServer, service.NewSalesProductService(cfg, log, strg, srvc))
	sell_service.RegisterShiftServiceServer(grpcServer, service.NewShiftService(cfg, log, strg, srvc))
	sell_service.RegisterPaymentServiceServer(grpcServer, service.NewPaymentService(cfg, log, strg, srvc))
	sell_service.RegisterTransactionServiceServer(grpcServer, service.NewTransactionService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
