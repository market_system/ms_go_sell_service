CREATE TABLE "transaction" (
  "id" UUID PRIMARY KEY NOT NULL,
  "cash" NUMERIC,
  "uzcard" NUMERIC,
  "payme" NUMERIC,
  "click" NUMERIC,
  "humo" NUMERIC,
  "apelsin" NUMERIC,
  "total_price" NUMERIC,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "shift" (
  "id" UUID PRIMARY KEY NOT NULL,
  "branch_id" UUID,
  "staff_id" UUID,
  "market_id" UUID,
  "status" varchar,
  "transaction_id" UUID,
  "check_id" varchar,
  "count" int,
  "price" NUMERIC,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "sale_product" (
  "id" UUID PRIMARY KEY NOT NULL,
  "brand_id" UUID,
  "product_id" UUID,
  "bar_code" varchar,
  "remainder_id" UUID,
  "sale_id" UUID,
  "count" int,
  "price" NUMERIC,
  "total_price" NUMERIC,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "sales" (
  "id" UUID PRIMARY KEY NOT NULL,
  "branch_id" UUID,
  "shift_id" UUID,
  "market_id" UUID,
  "staff_id" UUID,
  "status" varchar,
  "payment_id" UUID,
  "check_id" varchar,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

CREATE TABLE "payment" (
  "id" UUID PRIMARY KEY NOT NULL,
  "sale_id" UUID,
  "cash" NUMERIC,
  "uzcard" NUMERIC,
  "payme" NUMERIC,
  "click" NUMERIC,
  "humo" NUMERIC,
  "apelsin" NUMERIC,
  "visa" NUMERIC,
  "currency" varchar,
  "currency_count" NUMERIC,
  "exchange_sum" NUMERIC,
  "total_price" NUMERIC,
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP),
  "updated_at" timestamp
);

ALTER TABLE "shift" ADD FOREIGN KEY ("transaction_id") REFERENCES "transaction" ("id");

ALTER TABLE "sales" ADD FOREIGN KEY ("shift_id") REFERENCES "shift" ("id");

ALTER TABLE "sales" ADD FOREIGN KEY ("payment_id") REFERENCES "payment" ("id");

ALTER TABLE "sale_product" ADD FOREIGN KEY ("sale_id") REFERENCES "sales" ("id");

ALTER TABLE "payment" ADD FOREIGN KEY ("sale_id") REFERENCES "sales" ("id");