// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.19.6
// source: arrival_product.proto

package load_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	structpb "google.golang.org/protobuf/types/known/structpb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type ArrivalProduct struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	CategoryId string `protobuf:"bytes,2,opt,name=category_id,json=categoryId,proto3" json:"category_id,omitempty"`
	ProductId  string `protobuf:"bytes,3,opt,name=product_id,json=productId,proto3" json:"product_id,omitempty"`
	BrandId    string `protobuf:"bytes,4,opt,name=brand_id,json=brandId,proto3" json:"brand_id,omitempty"`
	Barcode    string `protobuf:"bytes,5,opt,name=barcode,proto3" json:"barcode,omitempty"`
	Count      int64  `protobuf:"varint,6,opt,name=count,proto3" json:"count,omitempty"`
	Price      int64  `protobuf:"varint,7,opt,name=price,proto3" json:"price,omitempty"`
	ArrivalId  string `protobuf:"bytes,8,opt,name=arrival_id,json=arrivalId,proto3" json:"arrival_id,omitempty"`
	CreatedAt  string `protobuf:"bytes,9,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt  string `protobuf:"bytes,10,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *ArrivalProduct) Reset() {
	*x = ArrivalProduct{}
	if protoimpl.UnsafeEnabled {
		mi := &file_arrival_product_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ArrivalProduct) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ArrivalProduct) ProtoMessage() {}

func (x *ArrivalProduct) ProtoReflect() protoreflect.Message {
	mi := &file_arrival_product_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ArrivalProduct.ProtoReflect.Descriptor instead.
func (*ArrivalProduct) Descriptor() ([]byte, []int) {
	return file_arrival_product_proto_rawDescGZIP(), []int{0}
}

func (x *ArrivalProduct) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *ArrivalProduct) GetCategoryId() string {
	if x != nil {
		return x.CategoryId
	}
	return ""
}

func (x *ArrivalProduct) GetProductId() string {
	if x != nil {
		return x.ProductId
	}
	return ""
}

func (x *ArrivalProduct) GetBrandId() string {
	if x != nil {
		return x.BrandId
	}
	return ""
}

func (x *ArrivalProduct) GetBarcode() string {
	if x != nil {
		return x.Barcode
	}
	return ""
}

func (x *ArrivalProduct) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *ArrivalProduct) GetPrice() int64 {
	if x != nil {
		return x.Price
	}
	return 0
}

func (x *ArrivalProduct) GetArrivalId() string {
	if x != nil {
		return x.ArrivalId
	}
	return ""
}

func (x *ArrivalProduct) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *ArrivalProduct) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type CreateArrivalProduct struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	BrandId    string `protobuf:"bytes,1,opt,name=brand_id,json=brandId,proto3" json:"brand_id,omitempty"`
	CategoryId string `protobuf:"bytes,2,opt,name=category_id,json=categoryId,proto3" json:"category_id,omitempty"`
	ProductId  string `protobuf:"bytes,3,opt,name=product_id,json=productId,proto3" json:"product_id,omitempty"`
	Price      int64  `protobuf:"varint,4,opt,name=price,proto3" json:"price,omitempty"`
	Count      int64  `protobuf:"varint,5,opt,name=count,proto3" json:"count,omitempty"`
	Barcode    string `protobuf:"bytes,6,opt,name=barcode,proto3" json:"barcode,omitempty"`
	ArrivalId  string `protobuf:"bytes,7,opt,name=arrival_id,json=arrivalId,proto3" json:"arrival_id,omitempty"`
}

func (x *CreateArrivalProduct) Reset() {
	*x = CreateArrivalProduct{}
	if protoimpl.UnsafeEnabled {
		mi := &file_arrival_product_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateArrivalProduct) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateArrivalProduct) ProtoMessage() {}

func (x *CreateArrivalProduct) ProtoReflect() protoreflect.Message {
	mi := &file_arrival_product_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateArrivalProduct.ProtoReflect.Descriptor instead.
func (*CreateArrivalProduct) Descriptor() ([]byte, []int) {
	return file_arrival_product_proto_rawDescGZIP(), []int{1}
}

func (x *CreateArrivalProduct) GetBrandId() string {
	if x != nil {
		return x.BrandId
	}
	return ""
}

func (x *CreateArrivalProduct) GetCategoryId() string {
	if x != nil {
		return x.CategoryId
	}
	return ""
}

func (x *CreateArrivalProduct) GetProductId() string {
	if x != nil {
		return x.ProductId
	}
	return ""
}

func (x *CreateArrivalProduct) GetPrice() int64 {
	if x != nil {
		return x.Price
	}
	return 0
}

func (x *CreateArrivalProduct) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *CreateArrivalProduct) GetBarcode() string {
	if x != nil {
		return x.Barcode
	}
	return ""
}

func (x *CreateArrivalProduct) GetArrivalId() string {
	if x != nil {
		return x.ArrivalId
	}
	return ""
}

type UpdateArrivalProduct struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	CategoryId string `protobuf:"bytes,2,opt,name=category_id,json=categoryId,proto3" json:"category_id,omitempty"`
	ProductId  string `protobuf:"bytes,3,opt,name=product_id,json=productId,proto3" json:"product_id,omitempty"`
	BrandId    string `protobuf:"bytes,4,opt,name=brand_id,json=brandId,proto3" json:"brand_id,omitempty"`
	Barcode    string `protobuf:"bytes,5,opt,name=barcode,proto3" json:"barcode,omitempty"`
	Count      int64  `protobuf:"varint,6,opt,name=count,proto3" json:"count,omitempty"`
	Price      int64  `protobuf:"varint,7,opt,name=price,proto3" json:"price,omitempty"`
	ArrivalId  string `protobuf:"bytes,8,opt,name=arrival_id,json=arrivalId,proto3" json:"arrival_id,omitempty"`
}

func (x *UpdateArrivalProduct) Reset() {
	*x = UpdateArrivalProduct{}
	if protoimpl.UnsafeEnabled {
		mi := &file_arrival_product_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateArrivalProduct) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateArrivalProduct) ProtoMessage() {}

func (x *UpdateArrivalProduct) ProtoReflect() protoreflect.Message {
	mi := &file_arrival_product_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateArrivalProduct.ProtoReflect.Descriptor instead.
func (*UpdateArrivalProduct) Descriptor() ([]byte, []int) {
	return file_arrival_product_proto_rawDescGZIP(), []int{2}
}

func (x *UpdateArrivalProduct) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdateArrivalProduct) GetCategoryId() string {
	if x != nil {
		return x.CategoryId
	}
	return ""
}

func (x *UpdateArrivalProduct) GetProductId() string {
	if x != nil {
		return x.ProductId
	}
	return ""
}

func (x *UpdateArrivalProduct) GetBrandId() string {
	if x != nil {
		return x.BrandId
	}
	return ""
}

func (x *UpdateArrivalProduct) GetBarcode() string {
	if x != nil {
		return x.Barcode
	}
	return ""
}

func (x *UpdateArrivalProduct) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *UpdateArrivalProduct) GetPrice() int64 {
	if x != nil {
		return x.Price
	}
	return 0
}

func (x *UpdateArrivalProduct) GetArrivalId() string {
	if x != nil {
		return x.ArrivalId
	}
	return ""
}

type UpdatePatchArrivalProduct struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id     string           `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Fields *structpb.Struct `protobuf:"bytes,2,opt,name=fields,proto3" json:"fields,omitempty"`
}

func (x *UpdatePatchArrivalProduct) Reset() {
	*x = UpdatePatchArrivalProduct{}
	if protoimpl.UnsafeEnabled {
		mi := &file_arrival_product_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdatePatchArrivalProduct) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdatePatchArrivalProduct) ProtoMessage() {}

func (x *UpdatePatchArrivalProduct) ProtoReflect() protoreflect.Message {
	mi := &file_arrival_product_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdatePatchArrivalProduct.ProtoReflect.Descriptor instead.
func (*UpdatePatchArrivalProduct) Descriptor() ([]byte, []int) {
	return file_arrival_product_proto_rawDescGZIP(), []int{3}
}

func (x *UpdatePatchArrivalProduct) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdatePatchArrivalProduct) GetFields() *structpb.Struct {
	if x != nil {
		return x.Fields
	}
	return nil
}

type ArrivalProductPrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *ArrivalProductPrimaryKey) Reset() {
	*x = ArrivalProductPrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_arrival_product_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ArrivalProductPrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ArrivalProductPrimaryKey) ProtoMessage() {}

func (x *ArrivalProductPrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_arrival_product_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ArrivalProductPrimaryKey.ProtoReflect.Descriptor instead.
func (*ArrivalProductPrimaryKey) Descriptor() ([]byte, []int) {
	return file_arrival_product_proto_rawDescGZIP(), []int{4}
}

func (x *ArrivalProductPrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type GetListArrivalProductRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset int64  `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit  int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	Search string `protobuf:"bytes,3,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *GetListArrivalProductRequest) Reset() {
	*x = GetListArrivalProductRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_arrival_product_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListArrivalProductRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListArrivalProductRequest) ProtoMessage() {}

func (x *GetListArrivalProductRequest) ProtoReflect() protoreflect.Message {
	mi := &file_arrival_product_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListArrivalProductRequest.ProtoReflect.Descriptor instead.
func (*GetListArrivalProductRequest) Descriptor() ([]byte, []int) {
	return file_arrival_product_proto_rawDescGZIP(), []int{5}
}

func (x *GetListArrivalProductRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *GetListArrivalProductRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *GetListArrivalProductRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

type GetListArrivalProductResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count           int64             `protobuf:"varint,1,opt,name=count,proto3" json:"count,omitempty"`
	ArrivalProducts []*ArrivalProduct `protobuf:"bytes,2,rep,name=ArrivalProducts,proto3" json:"ArrivalProducts,omitempty"`
}

func (x *GetListArrivalProductResponse) Reset() {
	*x = GetListArrivalProductResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_arrival_product_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListArrivalProductResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListArrivalProductResponse) ProtoMessage() {}

func (x *GetListArrivalProductResponse) ProtoReflect() protoreflect.Message {
	mi := &file_arrival_product_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListArrivalProductResponse.ProtoReflect.Descriptor instead.
func (*GetListArrivalProductResponse) Descriptor() ([]byte, []int) {
	return file_arrival_product_proto_rawDescGZIP(), []int{6}
}

func (x *GetListArrivalProductResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *GetListArrivalProductResponse) GetArrivalProducts() []*ArrivalProduct {
	if x != nil {
		return x.ArrivalProducts
	}
	return nil
}

var File_arrival_product_proto protoreflect.FileDescriptor

var file_arrival_product_proto_rawDesc = []byte{
	0x0a, 0x15, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x5f, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63,
	0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x0c, 0x6c, 0x6f, 0x61, 0x64, 0x5f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x1a, 0x1c, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2f, 0x73, 0x74, 0x72, 0x75, 0x63, 0x74, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x22, 0x9e, 0x02, 0x0a, 0x0e, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x50,
	0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f,
	0x72, 0x79, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x63, 0x61, 0x74,
	0x65, 0x67, 0x6f, 0x72, 0x79, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x70, 0x72, 0x6f, 0x64, 0x75,
	0x63, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x70, 0x72, 0x6f,
	0x64, 0x75, 0x63, 0x74, 0x49, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x64, 0x5f,
	0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x62, 0x72, 0x61, 0x6e, 0x64, 0x49,
	0x64, 0x12, 0x18, 0x0a, 0x07, 0x62, 0x61, 0x72, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x05, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x07, 0x62, 0x61, 0x72, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63,
	0x6f, 0x75, 0x6e, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e,
	0x74, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x72, 0x69, 0x63, 0x65, 0x18, 0x07, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x05, 0x70, 0x72, 0x69, 0x63, 0x65, 0x12, 0x1d, 0x0a, 0x0a, 0x61, 0x72, 0x72, 0x69, 0x76,
	0x61, 0x6c, 0x5f, 0x69, 0x64, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x61, 0x72, 0x72,
	0x69, 0x76, 0x61, 0x6c, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65,
	0x64, 0x5f, 0x61, 0x74, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61,
	0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64,
	0x5f, 0x61, 0x74, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74,
	0x65, 0x64, 0x41, 0x74, 0x22, 0xd6, 0x01, 0x0a, 0x14, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x41,
	0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x12, 0x19, 0x0a,
	0x08, 0x62, 0x72, 0x61, 0x6e, 0x64, 0x5f, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x07, 0x62, 0x72, 0x61, 0x6e, 0x64, 0x49, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x63, 0x61, 0x74, 0x65,
	0x67, 0x6f, 0x72, 0x79, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x63,
	0x61, 0x74, 0x65, 0x67, 0x6f, 0x72, 0x79, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x70, 0x72, 0x6f,
	0x64, 0x75, 0x63, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x70,
	0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x49, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x72, 0x69, 0x63,
	0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x70, 0x72, 0x69, 0x63, 0x65, 0x12, 0x14,
	0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x05, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x63,
	0x6f, 0x75, 0x6e, 0x74, 0x12, 0x18, 0x0a, 0x07, 0x62, 0x61, 0x72, 0x63, 0x6f, 0x64, 0x65, 0x18,
	0x06, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x62, 0x61, 0x72, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x1d,
	0x0a, 0x0a, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x5f, 0x69, 0x64, 0x18, 0x07, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x09, 0x61, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x49, 0x64, 0x22, 0xe6, 0x01,
	0x0a, 0x14, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x50,
	0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x1f, 0x0a, 0x0b, 0x63, 0x61, 0x74, 0x65, 0x67, 0x6f,
	0x72, 0x79, 0x5f, 0x69, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x09, 0x52, 0x0a, 0x63, 0x61, 0x74,
	0x65, 0x67, 0x6f, 0x72, 0x79, 0x49, 0x64, 0x12, 0x1d, 0x0a, 0x0a, 0x70, 0x72, 0x6f, 0x64, 0x75,
	0x63, 0x74, 0x5f, 0x69, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x70, 0x72, 0x6f,
	0x64, 0x75, 0x63, 0x74, 0x49, 0x64, 0x12, 0x19, 0x0a, 0x08, 0x62, 0x72, 0x61, 0x6e, 0x64, 0x5f,
	0x69, 0x64, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52, 0x07, 0x62, 0x72, 0x61, 0x6e, 0x64, 0x49,
	0x64, 0x12, 0x18, 0x0a, 0x07, 0x62, 0x61, 0x72, 0x63, 0x6f, 0x64, 0x65, 0x18, 0x05, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x07, 0x62, 0x61, 0x72, 0x63, 0x6f, 0x64, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63,
	0x6f, 0x75, 0x6e, 0x74, 0x18, 0x06, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e,
	0x74, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x72, 0x69, 0x63, 0x65, 0x18, 0x07, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x05, 0x70, 0x72, 0x69, 0x63, 0x65, 0x12, 0x1d, 0x0a, 0x0a, 0x61, 0x72, 0x72, 0x69, 0x76,
	0x61, 0x6c, 0x5f, 0x69, 0x64, 0x18, 0x08, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x61, 0x72, 0x72,
	0x69, 0x76, 0x61, 0x6c, 0x49, 0x64, 0x22, 0x5c, 0x0a, 0x19, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x50, 0x61, 0x74, 0x63, 0x68, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64,
	0x75, 0x63, 0x74, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x02, 0x69, 0x64, 0x12, 0x2f, 0x0a, 0x06, 0x66, 0x69, 0x65, 0x6c, 0x64, 0x73, 0x18, 0x02, 0x20,
	0x01, 0x28, 0x0b, 0x32, 0x17, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2e, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x75, 0x63, 0x74, 0x52, 0x06, 0x66, 0x69,
	0x65, 0x6c, 0x64, 0x73, 0x22, 0x2a, 0x0a, 0x18, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x50,
	0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b, 0x65, 0x79,
	0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64,
	0x22, 0x64, 0x0a, 0x1c, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x41, 0x72, 0x72, 0x69, 0x76,
	0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74,
	0x12, 0x16, 0x0a, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69,
	0x74, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x16,
	0x0a, 0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06,
	0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x22, 0x7d, 0x0a, 0x1d, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73,
	0x74, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x52,
	0x65, 0x73, 0x70, 0x6f, 0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74,
	0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x46, 0x0a,
	0x0f, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x73,
	0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x1c, 0x2e, 0x6c, 0x6f, 0x61, 0x64, 0x5f, 0x73, 0x65,
	0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x50, 0x72, 0x6f,
	0x64, 0x75, 0x63, 0x74, 0x52, 0x0f, 0x41, 0x72, 0x72, 0x69, 0x76, 0x61, 0x6c, 0x50, 0x72, 0x6f,
	0x64, 0x75, 0x63, 0x74, 0x73, 0x42, 0x17, 0x5a, 0x15, 0x67, 0x65, 0x6e, 0x70, 0x72, 0x6f, 0x74,
	0x6f, 0x2f, 0x6c, 0x6f, 0x61, 0x64, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x62, 0x06,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_arrival_product_proto_rawDescOnce sync.Once
	file_arrival_product_proto_rawDescData = file_arrival_product_proto_rawDesc
)

func file_arrival_product_proto_rawDescGZIP() []byte {
	file_arrival_product_proto_rawDescOnce.Do(func() {
		file_arrival_product_proto_rawDescData = protoimpl.X.CompressGZIP(file_arrival_product_proto_rawDescData)
	})
	return file_arrival_product_proto_rawDescData
}

var file_arrival_product_proto_msgTypes = make([]protoimpl.MessageInfo, 7)
var file_arrival_product_proto_goTypes = []interface{}{
	(*ArrivalProduct)(nil),                // 0: load_service.ArrivalProduct
	(*CreateArrivalProduct)(nil),          // 1: load_service.CreateArrivalProduct
	(*UpdateArrivalProduct)(nil),          // 2: load_service.UpdateArrivalProduct
	(*UpdatePatchArrivalProduct)(nil),     // 3: load_service.UpdatePatchArrivalProduct
	(*ArrivalProductPrimaryKey)(nil),      // 4: load_service.ArrivalProductPrimaryKey
	(*GetListArrivalProductRequest)(nil),  // 5: load_service.GetListArrivalProductRequest
	(*GetListArrivalProductResponse)(nil), // 6: load_service.GetListArrivalProductResponse
	(*structpb.Struct)(nil),               // 7: google.protobuf.Struct
}
var file_arrival_product_proto_depIdxs = []int32{
	7, // 0: load_service.UpdatePatchArrivalProduct.fields:type_name -> google.protobuf.Struct
	0, // 1: load_service.GetListArrivalProductResponse.ArrivalProducts:type_name -> load_service.ArrivalProduct
	2, // [2:2] is the sub-list for method output_type
	2, // [2:2] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_arrival_product_proto_init() }
func file_arrival_product_proto_init() {
	if File_arrival_product_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_arrival_product_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ArrivalProduct); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_arrival_product_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateArrivalProduct); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_arrival_product_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateArrivalProduct); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_arrival_product_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdatePatchArrivalProduct); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_arrival_product_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ArrivalProductPrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_arrival_product_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListArrivalProductRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_arrival_product_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListArrivalProductResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_arrival_product_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   7,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_arrival_product_proto_goTypes,
		DependencyIndexes: file_arrival_product_proto_depIdxs,
		MessageInfos:      file_arrival_product_proto_msgTypes,
	}.Build()
	File_arrival_product_proto = out.File
	file_arrival_product_proto_rawDesc = nil
	file_arrival_product_proto_goTypes = nil
	file_arrival_product_proto_depIdxs = nil
}
