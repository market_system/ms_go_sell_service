// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.19.6
// source: transaction.proto

package sell_service

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	structpb "google.golang.org/protobuf/types/known/structpb"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Transaction struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Cash       int64  `protobuf:"varint,2,opt,name=cash,proto3" json:"cash,omitempty"`
	Uzcard     int64  `protobuf:"varint,3,opt,name=uzcard,proto3" json:"uzcard,omitempty"`
	Payme      int64  `protobuf:"varint,4,opt,name=payme,proto3" json:"payme,omitempty"`
	Click      int64  `protobuf:"varint,5,opt,name=click,proto3" json:"click,omitempty"`
	Humo       int64  `protobuf:"varint,6,opt,name=humo,proto3" json:"humo,omitempty"`
	Apelsin    int64  `protobuf:"varint,7,opt,name=apelsin,proto3" json:"apelsin,omitempty"`
	TotalPrice int64  `protobuf:"varint,8,opt,name=total_price,json=totalPrice,proto3" json:"total_price,omitempty"`
	CreatedAt  string `protobuf:"bytes,9,opt,name=created_at,json=createdAt,proto3" json:"created_at,omitempty"`
	UpdatedAt  string `protobuf:"bytes,10,opt,name=updated_at,json=updatedAt,proto3" json:"updated_at,omitempty"`
}

func (x *Transaction) Reset() {
	*x = Transaction{}
	if protoimpl.UnsafeEnabled {
		mi := &file_transaction_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Transaction) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Transaction) ProtoMessage() {}

func (x *Transaction) ProtoReflect() protoreflect.Message {
	mi := &file_transaction_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Transaction.ProtoReflect.Descriptor instead.
func (*Transaction) Descriptor() ([]byte, []int) {
	return file_transaction_proto_rawDescGZIP(), []int{0}
}

func (x *Transaction) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *Transaction) GetCash() int64 {
	if x != nil {
		return x.Cash
	}
	return 0
}

func (x *Transaction) GetUzcard() int64 {
	if x != nil {
		return x.Uzcard
	}
	return 0
}

func (x *Transaction) GetPayme() int64 {
	if x != nil {
		return x.Payme
	}
	return 0
}

func (x *Transaction) GetClick() int64 {
	if x != nil {
		return x.Click
	}
	return 0
}

func (x *Transaction) GetHumo() int64 {
	if x != nil {
		return x.Humo
	}
	return 0
}

func (x *Transaction) GetApelsin() int64 {
	if x != nil {
		return x.Apelsin
	}
	return 0
}

func (x *Transaction) GetTotalPrice() int64 {
	if x != nil {
		return x.TotalPrice
	}
	return 0
}

func (x *Transaction) GetCreatedAt() string {
	if x != nil {
		return x.CreatedAt
	}
	return ""
}

func (x *Transaction) GetUpdatedAt() string {
	if x != nil {
		return x.UpdatedAt
	}
	return ""
}

type CreateTransaction struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Cash       int64 `protobuf:"varint,1,opt,name=cash,proto3" json:"cash,omitempty"`
	Uzcard     int64 `protobuf:"varint,2,opt,name=uzcard,proto3" json:"uzcard,omitempty"`
	Payme      int64 `protobuf:"varint,3,opt,name=payme,proto3" json:"payme,omitempty"`
	Click      int64 `protobuf:"varint,4,opt,name=click,proto3" json:"click,omitempty"`
	Humo       int64 `protobuf:"varint,5,opt,name=humo,proto3" json:"humo,omitempty"`
	Apelsin    int64 `protobuf:"varint,6,opt,name=apelsin,proto3" json:"apelsin,omitempty"`
	TotalPrice int64 `protobuf:"varint,7,opt,name=total_price,json=totalPrice,proto3" json:"total_price,omitempty"`
}

func (x *CreateTransaction) Reset() {
	*x = CreateTransaction{}
	if protoimpl.UnsafeEnabled {
		mi := &file_transaction_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *CreateTransaction) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*CreateTransaction) ProtoMessage() {}

func (x *CreateTransaction) ProtoReflect() protoreflect.Message {
	mi := &file_transaction_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use CreateTransaction.ProtoReflect.Descriptor instead.
func (*CreateTransaction) Descriptor() ([]byte, []int) {
	return file_transaction_proto_rawDescGZIP(), []int{1}
}

func (x *CreateTransaction) GetCash() int64 {
	if x != nil {
		return x.Cash
	}
	return 0
}

func (x *CreateTransaction) GetUzcard() int64 {
	if x != nil {
		return x.Uzcard
	}
	return 0
}

func (x *CreateTransaction) GetPayme() int64 {
	if x != nil {
		return x.Payme
	}
	return 0
}

func (x *CreateTransaction) GetClick() int64 {
	if x != nil {
		return x.Click
	}
	return 0
}

func (x *CreateTransaction) GetHumo() int64 {
	if x != nil {
		return x.Humo
	}
	return 0
}

func (x *CreateTransaction) GetApelsin() int64 {
	if x != nil {
		return x.Apelsin
	}
	return 0
}

func (x *CreateTransaction) GetTotalPrice() int64 {
	if x != nil {
		return x.TotalPrice
	}
	return 0
}

type UpdateTransaction struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id         string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Cash       int64  `protobuf:"varint,2,opt,name=cash,proto3" json:"cash,omitempty"`
	Uzcard     int64  `protobuf:"varint,3,opt,name=uzcard,proto3" json:"uzcard,omitempty"`
	Payme      int64  `protobuf:"varint,4,opt,name=payme,proto3" json:"payme,omitempty"`
	Click      int64  `protobuf:"varint,5,opt,name=click,proto3" json:"click,omitempty"`
	Humo       int64  `protobuf:"varint,6,opt,name=humo,proto3" json:"humo,omitempty"`
	Apelsin    int64  `protobuf:"varint,7,opt,name=apelsin,proto3" json:"apelsin,omitempty"`
	TotalPrice int64  `protobuf:"varint,8,opt,name=total_price,json=totalPrice,proto3" json:"total_price,omitempty"`
}

func (x *UpdateTransaction) Reset() {
	*x = UpdateTransaction{}
	if protoimpl.UnsafeEnabled {
		mi := &file_transaction_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdateTransaction) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdateTransaction) ProtoMessage() {}

func (x *UpdateTransaction) ProtoReflect() protoreflect.Message {
	mi := &file_transaction_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdateTransaction.ProtoReflect.Descriptor instead.
func (*UpdateTransaction) Descriptor() ([]byte, []int) {
	return file_transaction_proto_rawDescGZIP(), []int{2}
}

func (x *UpdateTransaction) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdateTransaction) GetCash() int64 {
	if x != nil {
		return x.Cash
	}
	return 0
}

func (x *UpdateTransaction) GetUzcard() int64 {
	if x != nil {
		return x.Uzcard
	}
	return 0
}

func (x *UpdateTransaction) GetPayme() int64 {
	if x != nil {
		return x.Payme
	}
	return 0
}

func (x *UpdateTransaction) GetClick() int64 {
	if x != nil {
		return x.Click
	}
	return 0
}

func (x *UpdateTransaction) GetHumo() int64 {
	if x != nil {
		return x.Humo
	}
	return 0
}

func (x *UpdateTransaction) GetApelsin() int64 {
	if x != nil {
		return x.Apelsin
	}
	return 0
}

func (x *UpdateTransaction) GetTotalPrice() int64 {
	if x != nil {
		return x.TotalPrice
	}
	return 0
}

type UpdatePatchTransaction struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id     string           `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Fields *structpb.Struct `protobuf:"bytes,2,opt,name=fields,proto3" json:"fields,omitempty"`
}

func (x *UpdatePatchTransaction) Reset() {
	*x = UpdatePatchTransaction{}
	if protoimpl.UnsafeEnabled {
		mi := &file_transaction_proto_msgTypes[3]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *UpdatePatchTransaction) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*UpdatePatchTransaction) ProtoMessage() {}

func (x *UpdatePatchTransaction) ProtoReflect() protoreflect.Message {
	mi := &file_transaction_proto_msgTypes[3]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use UpdatePatchTransaction.ProtoReflect.Descriptor instead.
func (*UpdatePatchTransaction) Descriptor() ([]byte, []int) {
	return file_transaction_proto_rawDescGZIP(), []int{3}
}

func (x *UpdatePatchTransaction) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *UpdatePatchTransaction) GetFields() *structpb.Struct {
	if x != nil {
		return x.Fields
	}
	return nil
}

type TransactionPrimaryKey struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *TransactionPrimaryKey) Reset() {
	*x = TransactionPrimaryKey{}
	if protoimpl.UnsafeEnabled {
		mi := &file_transaction_proto_msgTypes[4]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *TransactionPrimaryKey) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*TransactionPrimaryKey) ProtoMessage() {}

func (x *TransactionPrimaryKey) ProtoReflect() protoreflect.Message {
	mi := &file_transaction_proto_msgTypes[4]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use TransactionPrimaryKey.ProtoReflect.Descriptor instead.
func (*TransactionPrimaryKey) Descriptor() ([]byte, []int) {
	return file_transaction_proto_rawDescGZIP(), []int{4}
}

func (x *TransactionPrimaryKey) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

type GetListTransactionRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Offset int64  `protobuf:"varint,1,opt,name=offset,proto3" json:"offset,omitempty"`
	Limit  int64  `protobuf:"varint,2,opt,name=limit,proto3" json:"limit,omitempty"`
	Search string `protobuf:"bytes,3,opt,name=search,proto3" json:"search,omitempty"`
}

func (x *GetListTransactionRequest) Reset() {
	*x = GetListTransactionRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_transaction_proto_msgTypes[5]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListTransactionRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListTransactionRequest) ProtoMessage() {}

func (x *GetListTransactionRequest) ProtoReflect() protoreflect.Message {
	mi := &file_transaction_proto_msgTypes[5]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListTransactionRequest.ProtoReflect.Descriptor instead.
func (*GetListTransactionRequest) Descriptor() ([]byte, []int) {
	return file_transaction_proto_rawDescGZIP(), []int{5}
}

func (x *GetListTransactionRequest) GetOffset() int64 {
	if x != nil {
		return x.Offset
	}
	return 0
}

func (x *GetListTransactionRequest) GetLimit() int64 {
	if x != nil {
		return x.Limit
	}
	return 0
}

func (x *GetListTransactionRequest) GetSearch() string {
	if x != nil {
		return x.Search
	}
	return ""
}

type GetListTransactionResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Count        int64          `protobuf:"varint,1,opt,name=count,proto3" json:"count,omitempty"`
	Transactions []*Transaction `protobuf:"bytes,2,rep,name=Transactions,proto3" json:"Transactions,omitempty"`
}

func (x *GetListTransactionResponse) Reset() {
	*x = GetListTransactionResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_transaction_proto_msgTypes[6]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *GetListTransactionResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*GetListTransactionResponse) ProtoMessage() {}

func (x *GetListTransactionResponse) ProtoReflect() protoreflect.Message {
	mi := &file_transaction_proto_msgTypes[6]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use GetListTransactionResponse.ProtoReflect.Descriptor instead.
func (*GetListTransactionResponse) Descriptor() ([]byte, []int) {
	return file_transaction_proto_rawDescGZIP(), []int{6}
}

func (x *GetListTransactionResponse) GetCount() int64 {
	if x != nil {
		return x.Count
	}
	return 0
}

func (x *GetListTransactionResponse) GetTransactions() []*Transaction {
	if x != nil {
		return x.Transactions
	}
	return nil
}

var File_transaction_proto protoreflect.FileDescriptor

var file_transaction_proto_rawDesc = []byte{
	0x0a, 0x11, 0x74, 0x72, 0x61, 0x6e, 0x73, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x2e, 0x70, 0x72,
	0x6f, 0x74, 0x6f, 0x12, 0x0c, 0x73, 0x65, 0x6c, 0x6c, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x1a, 0x1c, 0x67, 0x6f, 0x6f, 0x67, 0x6c, 0x65, 0x2f, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62,
	0x75, 0x66, 0x2f, 0x73, 0x74, 0x72, 0x75, 0x63, 0x74, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22,
	0x82, 0x02, 0x0a, 0x0b, 0x54, 0x72, 0x61, 0x6e, 0x73, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x12,
	0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12,
	0x12, 0x0a, 0x04, 0x63, 0x61, 0x73, 0x68, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x04, 0x63,
	0x61, 0x73, 0x68, 0x12, 0x16, 0x0a, 0x06, 0x75, 0x7a, 0x63, 0x61, 0x72, 0x64, 0x18, 0x03, 0x20,
	0x01, 0x28, 0x03, 0x52, 0x06, 0x75, 0x7a, 0x63, 0x61, 0x72, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x70,
	0x61, 0x79, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x70, 0x61, 0x79, 0x6d,
	0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6c, 0x69, 0x63, 0x6b, 0x18, 0x05, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x05, 0x63, 0x6c, 0x69, 0x63, 0x6b, 0x12, 0x12, 0x0a, 0x04, 0x68, 0x75, 0x6d, 0x6f, 0x18,
	0x06, 0x20, 0x01, 0x28, 0x03, 0x52, 0x04, 0x68, 0x75, 0x6d, 0x6f, 0x12, 0x18, 0x0a, 0x07, 0x61,
	0x70, 0x65, 0x6c, 0x73, 0x69, 0x6e, 0x18, 0x07, 0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x61, 0x70,
	0x65, 0x6c, 0x73, 0x69, 0x6e, 0x12, 0x1f, 0x0a, 0x0b, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x5f, 0x70,
	0x72, 0x69, 0x63, 0x65, 0x18, 0x08, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0a, 0x74, 0x6f, 0x74, 0x61,
	0x6c, 0x50, 0x72, 0x69, 0x63, 0x65, 0x12, 0x1d, 0x0a, 0x0a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65,
	0x64, 0x5f, 0x61, 0x74, 0x18, 0x09, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x63, 0x72, 0x65, 0x61,
	0x74, 0x65, 0x64, 0x41, 0x74, 0x12, 0x1d, 0x0a, 0x0a, 0x75, 0x70, 0x64, 0x61, 0x74, 0x65, 0x64,
	0x5f, 0x61, 0x74, 0x18, 0x0a, 0x20, 0x01, 0x28, 0x09, 0x52, 0x09, 0x75, 0x70, 0x64, 0x61, 0x74,
	0x65, 0x64, 0x41, 0x74, 0x22, 0xba, 0x01, 0x0a, 0x11, 0x43, 0x72, 0x65, 0x61, 0x74, 0x65, 0x54,
	0x72, 0x61, 0x6e, 0x73, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x61,
	0x73, 0x68, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52, 0x04, 0x63, 0x61, 0x73, 0x68, 0x12, 0x16,
	0x0a, 0x06, 0x75, 0x7a, 0x63, 0x61, 0x72, 0x64, 0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06,
	0x75, 0x7a, 0x63, 0x61, 0x72, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x18,
	0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x12, 0x14, 0x0a, 0x05,
	0x63, 0x6c, 0x69, 0x63, 0x6b, 0x18, 0x04, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x63, 0x6c, 0x69,
	0x63, 0x6b, 0x12, 0x12, 0x0a, 0x04, 0x68, 0x75, 0x6d, 0x6f, 0x18, 0x05, 0x20, 0x01, 0x28, 0x03,
	0x52, 0x04, 0x68, 0x75, 0x6d, 0x6f, 0x12, 0x18, 0x0a, 0x07, 0x61, 0x70, 0x65, 0x6c, 0x73, 0x69,
	0x6e, 0x18, 0x06, 0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x61, 0x70, 0x65, 0x6c, 0x73, 0x69, 0x6e,
	0x12, 0x1f, 0x0a, 0x0b, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x5f, 0x70, 0x72, 0x69, 0x63, 0x65, 0x18,
	0x07, 0x20, 0x01, 0x28, 0x03, 0x52, 0x0a, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x50, 0x72, 0x69, 0x63,
	0x65, 0x22, 0xca, 0x01, 0x0a, 0x11, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x54, 0x72, 0x61, 0x6e,
	0x73, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20,
	0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x63, 0x61, 0x73, 0x68, 0x18,
	0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x04, 0x63, 0x61, 0x73, 0x68, 0x12, 0x16, 0x0a, 0x06, 0x75,
	0x7a, 0x63, 0x61, 0x72, 0x64, 0x18, 0x03, 0x20, 0x01, 0x28, 0x03, 0x52, 0x06, 0x75, 0x7a, 0x63,
	0x61, 0x72, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x18, 0x04, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x05, 0x70, 0x61, 0x79, 0x6d, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6c, 0x69,
	0x63, 0x6b, 0x18, 0x05, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x63, 0x6c, 0x69, 0x63, 0x6b, 0x12,
	0x12, 0x0a, 0x04, 0x68, 0x75, 0x6d, 0x6f, 0x18, 0x06, 0x20, 0x01, 0x28, 0x03, 0x52, 0x04, 0x68,
	0x75, 0x6d, 0x6f, 0x12, 0x18, 0x0a, 0x07, 0x61, 0x70, 0x65, 0x6c, 0x73, 0x69, 0x6e, 0x18, 0x07,
	0x20, 0x01, 0x28, 0x03, 0x52, 0x07, 0x61, 0x70, 0x65, 0x6c, 0x73, 0x69, 0x6e, 0x12, 0x1f, 0x0a,
	0x0b, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x5f, 0x70, 0x72, 0x69, 0x63, 0x65, 0x18, 0x08, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x0a, 0x74, 0x6f, 0x74, 0x61, 0x6c, 0x50, 0x72, 0x69, 0x63, 0x65, 0x22, 0x59,
	0x0a, 0x16, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65, 0x50, 0x61, 0x74, 0x63, 0x68, 0x54, 0x72, 0x61,
	0x6e, 0x73, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x2f, 0x0a, 0x06, 0x66, 0x69, 0x65, 0x6c,
	0x64, 0x73, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x17, 0x2e, 0x67, 0x6f, 0x6f, 0x67, 0x6c,
	0x65, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x62, 0x75, 0x66, 0x2e, 0x53, 0x74, 0x72, 0x75, 0x63,
	0x74, 0x52, 0x06, 0x66, 0x69, 0x65, 0x6c, 0x64, 0x73, 0x22, 0x27, 0x0a, 0x15, 0x54, 0x72, 0x61,
	0x6e, 0x73, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x50, 0x72, 0x69, 0x6d, 0x61, 0x72, 0x79, 0x4b,
	0x65, 0x79, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02,
	0x69, 0x64, 0x22, 0x61, 0x0a, 0x19, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74, 0x54, 0x72, 0x61,
	0x6e, 0x73, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12,
	0x16, 0x0a, 0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x18, 0x01, 0x20, 0x01, 0x28, 0x03, 0x52,
	0x06, 0x6f, 0x66, 0x66, 0x73, 0x65, 0x74, 0x12, 0x14, 0x0a, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x03, 0x52, 0x05, 0x6c, 0x69, 0x6d, 0x69, 0x74, 0x12, 0x16, 0x0a,
	0x06, 0x73, 0x65, 0x61, 0x72, 0x63, 0x68, 0x18, 0x03, 0x20, 0x01, 0x28, 0x09, 0x52, 0x06, 0x73,
	0x65, 0x61, 0x72, 0x63, 0x68, 0x22, 0x71, 0x0a, 0x1a, 0x47, 0x65, 0x74, 0x4c, 0x69, 0x73, 0x74,
	0x54, 0x72, 0x61, 0x6e, 0x73, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x65, 0x73, 0x70, 0x6f,
	0x6e, 0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x03, 0x52, 0x05, 0x63, 0x6f, 0x75, 0x6e, 0x74, 0x12, 0x3d, 0x0a, 0x0c, 0x54, 0x72, 0x61,
	0x6e, 0x73, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x0b, 0x32,
	0x19, 0x2e, 0x73, 0x65, 0x6c, 0x6c, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63, 0x65, 0x2e, 0x54,
	0x72, 0x61, 0x6e, 0x73, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x52, 0x0c, 0x54, 0x72, 0x61, 0x6e,
	0x73, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x73, 0x42, 0x17, 0x5a, 0x15, 0x67, 0x65, 0x6e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x2f, 0x73, 0x65, 0x6c, 0x6c, 0x5f, 0x73, 0x65, 0x72, 0x76, 0x69, 0x63,
	0x65, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_transaction_proto_rawDescOnce sync.Once
	file_transaction_proto_rawDescData = file_transaction_proto_rawDesc
)

func file_transaction_proto_rawDescGZIP() []byte {
	file_transaction_proto_rawDescOnce.Do(func() {
		file_transaction_proto_rawDescData = protoimpl.X.CompressGZIP(file_transaction_proto_rawDescData)
	})
	return file_transaction_proto_rawDescData
}

var file_transaction_proto_msgTypes = make([]protoimpl.MessageInfo, 7)
var file_transaction_proto_goTypes = []interface{}{
	(*Transaction)(nil),                // 0: sell_service.Transaction
	(*CreateTransaction)(nil),          // 1: sell_service.CreateTransaction
	(*UpdateTransaction)(nil),          // 2: sell_service.UpdateTransaction
	(*UpdatePatchTransaction)(nil),     // 3: sell_service.UpdatePatchTransaction
	(*TransactionPrimaryKey)(nil),      // 4: sell_service.TransactionPrimaryKey
	(*GetListTransactionRequest)(nil),  // 5: sell_service.GetListTransactionRequest
	(*GetListTransactionResponse)(nil), // 6: sell_service.GetListTransactionResponse
	(*structpb.Struct)(nil),            // 7: google.protobuf.Struct
}
var file_transaction_proto_depIdxs = []int32{
	7, // 0: sell_service.UpdatePatchTransaction.fields:type_name -> google.protobuf.Struct
	0, // 1: sell_service.GetListTransactionResponse.Transactions:type_name -> sell_service.Transaction
	2, // [2:2] is the sub-list for method output_type
	2, // [2:2] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_transaction_proto_init() }
func file_transaction_proto_init() {
	if File_transaction_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_transaction_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Transaction); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_transaction_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*CreateTransaction); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_transaction_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdateTransaction); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_transaction_proto_msgTypes[3].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*UpdatePatchTransaction); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_transaction_proto_msgTypes[4].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*TransactionPrimaryKey); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_transaction_proto_msgTypes[5].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListTransactionRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_transaction_proto_msgTypes[6].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*GetListTransactionResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_transaction_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   7,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_transaction_proto_goTypes,
		DependencyIndexes: file_transaction_proto_depIdxs,
		MessageInfos:      file_transaction_proto_msgTypes,
	}.Build()
	File_transaction_proto = out.File
	file_transaction_proto_rawDesc = nil
	file_transaction_proto_goTypes = nil
	file_transaction_proto_depIdxs = nil
}
