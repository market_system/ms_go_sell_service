package storage

import (
	"context"

	"gitlab.com/market_system/ms_go_sell_service/genproto/sell_service"
	"gitlab.com/market_system/ms_go_sell_service/models"
	"google.golang.org/protobuf/types/known/emptypb"
)

type StorageI interface {
	CloseDB()
	Sales() SalesRepoI
	SalesProduct() SalesProductRepoI
	Payment() PaymentRepoI
	Shift() ShiftRepoI
	Transaction() TransactionRepoI
}

type SalesRepoI interface {
	Create(ctx context.Context, req *sell_service.CreateSales) (resp *sell_service.SalesPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *sell_service.SalesPrimaryKey) (resp *sell_service.Sales, err error)
	GetAll(ctx context.Context, req *sell_service.GetListSalesRequest) (resp *sell_service.GetListSalesResponse, err error)
	Update(ctx context.Context, req *sell_service.UpdateSales) (rowsAffected int64, err error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *sell_service.SalesPrimaryKey) (*emptypb.Empty, error)
}

type SalesProductRepoI interface {
	Create(ctx context.Context, req *sell_service.CreateSalesProduct) (resp *sell_service.SalesProductPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *sell_service.SalesProductPrimaryKey) (resp *sell_service.SalesProduct, err error)
	GetAll(ctx context.Context, req *sell_service.GetListSalesProductRequest) (resp *sell_service.GetListSalesProductResponse, err error)
	Update(ctx context.Context, req *sell_service.UpdateSalesProduct) (rowsAffected int64, err error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *sell_service.SalesProductPrimaryKey) (*emptypb.Empty, error)
}
type PaymentRepoI interface {
	Create(ctx context.Context, req *sell_service.CreatePayment) (resp *sell_service.PaymentPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *sell_service.PaymentPrimaryKey) (resp *sell_service.Payment, err error)
	GetAll(ctx context.Context, req *sell_service.GetListPaymentRequest) (resp *sell_service.GetListPaymentResponse, err error)
	Update(ctx context.Context, req *sell_service.UpdatePayment) (rowsAffected int64, err error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *sell_service.PaymentPrimaryKey) (*emptypb.Empty, error)
}
type ShiftRepoI interface {
	Create(ctx context.Context, req *sell_service.CreateShift) (resp *sell_service.ShiftPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *sell_service.ShiftPrimaryKey) (resp *sell_service.Shift, err error)
	GetAll(ctx context.Context, req *sell_service.GetListShiftRequest) (resp *sell_service.GetListShiftResponse, err error)
	Update(ctx context.Context, req *sell_service.UpdateShift) (rowsAffected int64, err error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *sell_service.ShiftPrimaryKey) error
}

type TransactionRepoI interface {
	Create(ctx context.Context, req *sell_service.CreateTransaction) (resp *sell_service.TransactionPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *sell_service.TransactionPrimaryKey) (resp *sell_service.Transaction, err error)
	GetAll(ctx context.Context, req *sell_service.GetListTransactionRequest) (resp *sell_service.GetListTransactionResponse, err error)
	Update(ctx context.Context, req *sell_service.UpdateTransaction) (rowsAffected int64, err error)
	Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *sell_service.TransactionPrimaryKey) (*emptypb.Empty, error)
}
