package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_sell_service/genproto/sell_service"
	"gitlab.com/market_system/ms_go_sell_service/models"
	"gitlab.com/market_system/ms_go_sell_service/pkg/helper"
	"gitlab.com/market_system/ms_go_sell_service/storage"
	"google.golang.org/protobuf/types/known/emptypb"
)

type SalesProductRepo struct {
	db *pgxpool.Pool
}

func NewSalesProductRepo(db *pgxpool.Pool) storage.SalesProductRepoI {
	return &SalesProductRepo{
		db: db,
	}
}

func (c *SalesProductRepo) Create(ctx context.Context, req *sell_service.CreateSalesProduct) (resp *sell_service.SalesProductPrimaryKey, err error) {
	var id = uuid.New()
	query := `INSERT INTO "sale_product" (
				id,
				brand_id,
				product_id,
				bar_code,
				remainder_id,
				count,
				price,
				total_price,
				sale_id

			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.BrandId,
		req.ProductId,
		req.BarCode,
		req.RemainderId,
		req.Count,
		req.Price,
		req.TotalPrice,
		req.SaleId,
	)
	if err != nil {
		return nil, err
	}

	return &sell_service.SalesProductPrimaryKey{Id: id.String()}, nil

}

func (c *SalesProductRepo) GetByPKey(ctx context.Context, req *sell_service.SalesProductPrimaryKey) (resp *sell_service.SalesProduct, err error) {
	query := `
		SELECT
			id,
			brand_id,
			product_id,
			bar_code,
			remainder_id,
			count,
			price,
			total_price,  
			sale_id, 
			created_at,
			updated_at
		FROM "sale_product"
		WHERE id = $1
	`

	var (
		id           sql.NullString
		brand_id     sql.NullString
		product_id   sql.NullString
		bar_code     sql.NullString
		remainder_id sql.NullString
		count        int
		price        int
		total_price  int
		sale_id      sql.NullString
		createdAt    sql.NullString
		updatedAt    sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&brand_id,
		&product_id,
		&bar_code,
		&remainder_id,
		&count,
		&price,
		&total_price,
		&sale_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &sell_service.SalesProduct{
		Id:          id.String,
		BrandId:     brand_id.String,
		ProductId:   product_id.String,
		BarCode:     bar_code.String,
		RemainderId: remainder_id.String,
		Count:       int64(count),
		Price:       int64(price),
		TotalPrice:  int64(total_price),
		SaleId:      sale_id.String,
		CreatedAt:   createdAt.String,
		UpdatedAt:   updatedAt.String,
	}

	return
}

func (c *SalesProductRepo) GetAll(ctx context.Context, req *sell_service.GetListSalesProductRequest) (resp *sell_service.GetListSalesProductResponse, err error) {

	resp = &sell_service.GetListSalesProductResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter string
		sort   = " ORDER BY created_at DESC"
	)

	if len(req.SaleId) > 0 && helper.IsValidUUID(req.SaleId) {
		filter = " WHERE sale_id = '" + req.SaleId + "' "
	} else if len(req.ProductId) > 0 && helper.IsValidUUID(req.ProductId) {
		filter += " AND product_id = '" + req.ProductId + "' "
	} else {
		filter = " WHERE TRUE "
	}

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			brand_id,
			product_id,
			bar_code,
			remainder_id,
			count,
			price,
			total_price,
			sale_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "sale_product" 
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)

	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id           sql.NullString
			brand_id     sql.NullString
			product_id   sql.NullString
			bar_code     sql.NullString
			remainder_id sql.NullString
			count        int
			price        int
			total_price  int
			sale_id      sql.NullString
			createdAt    sql.NullString
			updatedAt    sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&brand_id,
			&product_id,
			&bar_code,
			&remainder_id,
			&count,
			&price,
			&total_price,
			&sale_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.SalesProducts = append(resp.SalesProducts, &sell_service.SalesProduct{
			Id:          id.String,
			BrandId:     brand_id.String,
			ProductId:   product_id.String,
			BarCode:     bar_code.String,
			RemainderId: remainder_id.String,
			Count:       int64(count),
			Price:       int64(price),
			TotalPrice:  int64(total_price),
			SaleId:      sale_id.String,
			CreatedAt:   createdAt.String,
			UpdatedAt:   updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *SalesProductRepo) Update(ctx context.Context, req *sell_service.UpdateSalesProduct) (rowsAffected int64, err error) {

	var (
		query    string
		queryRow string
		price    int64
		params   map[string]interface{}
	)

	query = `
			UPDATE
			    "sale_product"
			SET
				product_id = :product_id,
				count = :count,
				price = :price,
				updated_at = now()
			WHERE
				id = :id`

	queryRow = `
			SELECT
				price
			FROM "sale_product"
			WHERE id = $1
	`

	err = c.db.QueryRow(ctx, queryRow, req.Id).Scan(&price)
	if err != nil {
		return
	}

	price *= req.GetCount()

	params = map[string]interface{}{
		"id":         req.GetId(),
		"product_id": req.GetProductId(),
		"count":      req.GetCount(),
		"price":      price,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (r *SalesProductRepo) Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"sale_product"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err

}

func (c *SalesProductRepo) Delete(ctx context.Context, req *sell_service.SalesProductPrimaryKey) (resp *emptypb.Empty, err error) {

	query := `DELETE FROM "sale_product" WHERE id = $1`

	_, err = c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return
}
