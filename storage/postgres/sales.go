package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_sell_service/genproto/sell_service"
	"gitlab.com/market_system/ms_go_sell_service/models"
	"gitlab.com/market_system/ms_go_sell_service/pkg/helper"
	"gitlab.com/market_system/ms_go_sell_service/storage"
	"google.golang.org/protobuf/types/known/emptypb"
)

type SalesRepo struct {
	db *pgxpool.Pool
}

func NewSalesRepo(db *pgxpool.Pool) storage.SalesRepoI {
	return &SalesRepo{
		db: db,
	}
}

func (c *SalesRepo) Create(ctx context.Context, req *sell_service.CreateSales) (resp *sell_service.SalesPrimaryKey, err error) {
	var (
		id          = uuid.New()
		lastCheckID string
	)
	queryRow := `
		SELECT 
			check_id
		FROM "sales"
		ORDER BY created_at DESC
		LIMIT 1

	`

	err = c.db.QueryRow(ctx, queryRow).Scan(&lastCheckID)
	checkID := helper.GenerateID(lastCheckID, "M")

	query := `INSERT INTO "sales" (
				id,
				branch_id,
				shift_id,
				market_id,
				staff_id,
				status,
				check_id

			) VALUES ($1, $2, $3, $4, $5, $6, $7)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.BranchId,
		req.ShiftId,
		req.MarketId,
		req.StaffId,
		req.Status,
		checkID,
	)
	if err != nil {
		return nil, err
	}

	return &sell_service.SalesPrimaryKey{Id: id.String()}, nil

}

func (c *SalesRepo) GetByPKey(ctx context.Context, req *sell_service.SalesPrimaryKey) (resp *sell_service.Sales, err error) {
	query := `
		SELECT
			id,
			branch_id,
			shift_id,
			market_id,
			staff_id,
			status,
			payment_id, 
			check_id,   
			created_at,
			updated_at
		FROM "sales"
		WHERE id = $1
	`

	var (
		id         sql.NullString
		branch_id  sql.NullString
		shift_id   sql.NullString
		market_id  sql.NullString
		staff_id   sql.NullString
		status     sql.NullString
		payment_id sql.NullString
		check_id   sql.NullString
		createdAt  sql.NullString
		updatedAt  sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_id,
		&shift_id,
		&market_id,
		&staff_id,
		&status,
		&payment_id,
		&check_id,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &sell_service.Sales{
		Id:        id.String,
		BranchId:  branch_id.String,
		ShiftId:   shift_id.String,
		MarketId:  market_id.String,
		StaffId:   staff_id.String,
		Status:    status.String,
		PaymentId: payment_id.String,
		CheckId:   check_id.String,
		CreatedAt: createdAt.String,
		UpdatedAt: updatedAt.String,
	}

	return
}

func (c *SalesRepo) GetAll(ctx context.Context, req *sell_service.GetListSalesRequest) (resp *sell_service.GetListSalesResponse, err error) {

	resp = &sell_service.GetListSalesResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			shift_id,
			market_id,
			staff_id,
			status,
			payment_id,
			check_id,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "sales"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id         sql.NullString
			branch_id  sql.NullString
			shift_id   sql.NullString
			market_id  sql.NullString
			staff_id   sql.NullString
			status     sql.NullString
			payment_id sql.NullString
			check_id   sql.NullString
			createdAt  sql.NullString
			updatedAt  sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_id,
			&shift_id,
			&market_id,
			&staff_id,
			&status,
			&payment_id,
			&check_id,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Saless = append(resp.Saless, &sell_service.Sales{
			Id:        id.String,
			BranchId:  branch_id.String,
			ShiftId:   shift_id.String,
			MarketId:  market_id.String,
			StaffId:   staff_id.String,
			Status:    status.String,
			PaymentId: payment_id.String,
			CheckId:   check_id.String,
			CreatedAt: createdAt.String,
			UpdatedAt: updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *SalesRepo) Update(ctx context.Context, req *sell_service.UpdateSales) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "sales"
			SET
				branch_id = :branch_id,
				shift_id = :shift_id,
				market_id = :market_id,
				staff_id = :staff_id,
				status = :status,
				payment_id = :payment_id,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":         req.GetId(),
		"branch_id":  req.GetBranchId(),
		"shift_id":   req.GetShiftId(),
		"market_id":  req.GetMarketId(),
		"staff_id":   req.GetStaffId(),
		"status":     req.GetStatus(),
		"payment_id": req.GetPaymentId(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (r *SalesRepo) Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"sales"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err

}

func (c *SalesRepo) Delete(ctx context.Context, req *sell_service.SalesPrimaryKey) (resp *emptypb.Empty, err error) {

	query := `DELETE FROM "sales" WHERE id = $1`

	_, err = c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return
}
