package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_sell_service/genproto/sell_service"
	"gitlab.com/market_system/ms_go_sell_service/models"
	"gitlab.com/market_system/ms_go_sell_service/pkg/helper"
	"gitlab.com/market_system/ms_go_sell_service/storage"
)

type ShiftRepo struct {
	db *pgxpool.Pool
}

func NewShiftRepo(db *pgxpool.Pool) storage.ShiftRepoI {
	return &ShiftRepo{
		db: db,
	}
}

func (c *ShiftRepo) Create(ctx context.Context, req *sell_service.CreateShift) (resp *sell_service.ShiftPrimaryKey, err error) {
	var (
		id          = uuid.New()
		count       int
		lastCheckID string
	)

	queryRow := `
		SELECT 
			check_id
		FROM "shift"
		WHERE branch_id = $1
		ORDER BY created_at DESC
		LIMIT 1

	`

	err = c.db.QueryRow(ctx, queryRow, req.BranchId).Scan(&lastCheckID)
	checkID := helper.GenerateID(lastCheckID, "S")

	query1 := `
		SELECT 
			COUNT(*) OVER()
		FROM "shift"
		WHERE status = $1 AND branch_id = $2
		`

	err = c.db.QueryRow(ctx, query1, req.Status, req.BranchId).Scan(&count)
	if count > 0 {
		return nil, errors.New("There is an opened shift in this filial. Please, close it.")
	}

	query := `INSERT INTO "shift" (
				id,
				branch_id,
				staff_id,
				market_id,
				check_id,
				status,
				count,
				price

			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.BranchId,
		req.StaffId,
		req.MarketId,
		checkID,
		req.Status,
		0,
		0,
	)
	if err != nil {
		return nil, err
	}

	return &sell_service.ShiftPrimaryKey{Id: id.String()}, nil

}

func (c *ShiftRepo) GetByPKey(ctx context.Context, req *sell_service.ShiftPrimaryKey) (resp *sell_service.Shift, err error) {
	query := `
		SELECT
			id,
			branch_id,
			staff_id,
			market_id,
			status,
			transaction_id,
			check_id,
			count,
			price,    
			created_at,
			updated_at
		FROM "shift"
		WHERE id = $1
	`

	var (
		id             sql.NullString
		branch_id      sql.NullString
		staff_id       sql.NullString
		market_id      sql.NullString
		status         sql.NullString
		transaction_id sql.NullString
		check_id       sql.NullString
		count          int64
		price          int64
		createdAt      sql.NullString
		updatedAt      sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&branch_id,
		&staff_id,
		&market_id,
		&status,
		&transaction_id,
		&check_id,
		&count,
		&price,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &sell_service.Shift{
		Id:            id.String,
		BranchId:      branch_id.String,
		StaffId:       staff_id.String,
		MarketId:      market_id.String,
		Status:        status.String,
		TransactionId: transaction_id.String,
		CheckId:       check_id.String,
		Count:         count,
		Price:         price,
		CreatedAt:     createdAt.String,
		UpdatedAt:     updatedAt.String,
	}

	return
}

func (c *ShiftRepo) GetAll(ctx context.Context, req *sell_service.GetListShiftRequest) (resp *sell_service.GetListShiftResponse, err error) {

	resp = &sell_service.GetListShiftResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			branch_id,
			staff_id,
			market_id,
			status,
			transaction_id,
			check_id,
			count,
			price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "shift"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id             sql.NullString
			branch_id      sql.NullString
			staff_id       sql.NullString
			market_id      sql.NullString
			status         sql.NullString
			transaction_id sql.NullString
			check_id       sql.NullString
			count          int64
			price          int64
			createdAt      sql.NullString
			updatedAt      sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&branch_id,
			&staff_id,
			&market_id,
			&status,
			&transaction_id,
			&check_id,
			&count,
			&price,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Shifts = append(resp.Shifts, &sell_service.Shift{
			Id:            id.String,
			BranchId:      branch_id.String,
			StaffId:       staff_id.String,
			MarketId:      market_id.String,
			Status:        status.String,
			TransactionId: transaction_id.String,
			CheckId:       check_id.String,
			Count:         count,
			Price:         price,
			CreatedAt:     createdAt.String,
			UpdatedAt:     updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *ShiftRepo) Update(ctx context.Context, req *sell_service.UpdateShift) (rowsAffected int64, err error) {

	var (
		query  string
		params map[string]interface{}
	)

	query = `
			UPDATE
			    "shift"
			SET
				branch_id = :branch_id,
				staff_id = :staff_id,
				market_id = :market_id,
				transaction_id = :transaction_id,
				count = :count,
				price = :price,
				status = :status,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":             req.GetId(),
		"branch_id":      req.GetBranchId(),
		"staff_id":       req.GetStaffId(),
		"market_id":      req.GetMarketId(),
		"transaction_id": req.GetTransactionId(),
		"status":         req.GetStatus(),
		"count":          req.GetCount(),
		"price":          req.GetPrice(),
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}
	return result.RowsAffected(), nil
}

func (r *ShiftRepo) Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"shift"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err

}

func (c *ShiftRepo) Delete(ctx context.Context, req *sell_service.ShiftPrimaryKey) error {

	query := `DELETE FROM "shift" WHERE id = $1`

	_, err := c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return err
	}

	return nil
}
