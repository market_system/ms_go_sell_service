package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_sell_service/genproto/sell_service"
	"gitlab.com/market_system/ms_go_sell_service/models"
	"gitlab.com/market_system/ms_go_sell_service/pkg/helper"
	"gitlab.com/market_system/ms_go_sell_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type TransactionRepo struct {
	db *pgxpool.Pool
}

func NewTransactionRepo(db *pgxpool.Pool) storage.TransactionRepoI {
	return &TransactionRepo{
		db: db,
	}
}

func (c *TransactionRepo) Create(ctx context.Context, req *sell_service.CreateTransaction) (resp *sell_service.TransactionPrimaryKey, err error) {
	var (
		id             = uuid.New()
		allTransaction int
	)
	allTransaction = int(req.Cash) + int(req.Uzcard) + int(req.Payme) + int(req.Click) + int(req.Humo) + int(req.Apelsin)
	query := `INSERT INTO "transaction" (
				id,
				cash,
				uzcard,
				payme,
				click,
				humo,
				apelsin,
				total_price

			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Cash,
		req.Uzcard,
		req.Payme,
		req.Click,
		req.Humo,
		req.Apelsin,
		allTransaction,
	)
	if err != nil {
		return nil, err
	}

	return &sell_service.TransactionPrimaryKey{Id: id.String()}, nil

}

func (c *TransactionRepo) GetByPKey(ctx context.Context, req *sell_service.TransactionPrimaryKey) (resp *sell_service.Transaction, err error) {
	query := `
		SELECT
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			total_price,    
			created_at,
			updated_at
		FROM "transaction"
		WHERE id = $1
	`

	var (
		id          sql.NullString
		cash        int64
		uzcard      int64
		payme       int64
		click       int64
		humo        int64
		apelsin     int64
		total_price int64
		createdAt   sql.NullString
		updatedAt   sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&cash,
		&uzcard,
		&payme,
		&click,
		&humo,
		&apelsin,
		&total_price,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &sell_service.Transaction{
		Id:         id.String,
		Cash:       cash,
		Uzcard:     uzcard,
		Payme:      payme,
		Click:      click,
		Humo:       humo,
		Apelsin:    apelsin,
		TotalPrice: total_price,
		CreatedAt:  createdAt.String,
		UpdatedAt:  updatedAt.String,
	}

	return
}

func (c *TransactionRepo) GetAll(ctx context.Context, req *sell_service.GetListTransactionRequest) (resp *sell_service.GetListTransactionResponse, err error) {

	resp = &sell_service.GetListTransactionResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
				id,
				cash,
				uzcard,
				payme,
				click,
				humo,
				apelsin,
				total_price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "transaction"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id          sql.NullString
			cash        int64
			uzcard      int64
			payme       int64
			click       int64
			humo        int64
			apelsin     int64
			total_price int64
			createdAt   sql.NullString
			updatedAt   sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&cash,
			&uzcard,
			&payme,
			&click,
			&humo,
			&apelsin,
			&total_price,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Transactions = append(resp.Transactions, &sell_service.Transaction{
			Id:         id.String,
			Cash:       cash,
			Uzcard:     uzcard,
			Payme:      payme,
			Click:      click,
			Humo:       humo,
			Apelsin:    apelsin,
			TotalPrice: total_price,
			CreatedAt:  createdAt.String,
			UpdatedAt:  updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *TransactionRepo) Update(ctx context.Context, req *sell_service.UpdateTransaction) (rowsAffected int64, err error) {

	var (
		query          string
		params         map[string]interface{}
		allTransaction int
	)
	allTransaction = int(req.Cash) + int(req.Uzcard) + int(req.Payme) + int(req.Click) + int(req.Humo) + int(req.Apelsin)

	query = `
			UPDATE
			    "transaction"
			SET
				cash = :cash,
				uzcard = :uzcard,
				payme = :payme,
				click = :click,
				humo = :humo,
				apelsin = :apelsin,
				total_price = :total_price,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":          req.GetId(),
		"cash":        req.GetCash(),
		"uzcard":      req.GetUzcard(),
		"payme":       req.GetPayme(),
		"click":       req.GetClick(),
		"humo":        req.GetHumo(),
		"apelsin":     req.GetApelsin(),
		"total_price": allTransaction,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (r *TransactionRepo) Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"transaction"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err

}

func (c *TransactionRepo) Delete(ctx context.Context, req *sell_service.TransactionPrimaryKey) (resp *emptypb.Empty, err error) {

	query := `DELETE FROM "transaction" WHERE id = $1`

	_, err = c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return
}
