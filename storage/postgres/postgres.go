package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_sell_service/config"
	"gitlab.com/market_system/ms_go_sell_service/storage"
)

type Store struct {
	db           *pgxpool.Pool
	sales        storage.SalesRepoI
	salesProduct storage.SalesProductRepoI
	payment      storage.PaymentRepoI
	shift        storage.ShiftRepoI
	transaction  storage.TransactionRepoI
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",

			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)

	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.ConnectConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, err

}
func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Sales() storage.SalesRepoI {

	if s.sales == nil {
		s.sales = NewSalesRepo(s.db)
	}

	return s.sales
}

func (s *Store) SalesProduct() storage.SalesProductRepoI {

	if s.salesProduct == nil {
		s.salesProduct = NewSalesProductRepo(s.db)
	}

	return s.salesProduct
}

func (s *Store) Payment() storage.PaymentRepoI {

	if s.payment == nil {
		s.payment = NewPaymentRepo(s.db)
	}

	return s.payment
}

func (s *Store) Shift() storage.ShiftRepoI {

	if s.shift == nil {
		s.shift = NewShiftRepo(s.db)
	}

	return s.shift
}

func (s *Store) Transaction() storage.TransactionRepoI {

	if s.transaction == nil {
		s.transaction = NewTransactionRepo(s.db)
	}

	return s.transaction
}
