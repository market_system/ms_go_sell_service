package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.com/market_system/ms_go_sell_service/genproto/sell_service"
	"gitlab.com/market_system/ms_go_sell_service/models"
	"gitlab.com/market_system/ms_go_sell_service/pkg/helper"
	"gitlab.com/market_system/ms_go_sell_service/storage"

	"google.golang.org/protobuf/types/known/emptypb"
)

type PaymentRepo struct {
	db *pgxpool.Pool
}

func NewPaymentRepo(db *pgxpool.Pool) storage.PaymentRepoI {
	return &PaymentRepo{
		db: db,
	}
}

func (c *PaymentRepo) Create(ctx context.Context, req *sell_service.CreatePayment) (resp *sell_service.PaymentPrimaryKey, err error) {
	var (
		id         = uuid.New()
		allPayment int
	)
	allPayment = int(req.Cash) + int(req.Uzcard) + int(req.Payme) + int(req.Click) + int(req.Humo) + int(req.Apelsin) + int(req.Visa) + int(req.CurrencyCount)*int(req.ExchangeSum)
	query := `INSERT INTO "payment" (
				id,
				cash,
				uzcard,
				payme,
				click,
				humo,
				apelsin,
				visa,
				currency,
				currency_count,
				exchange_sum,
				total_price

			) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11,$12)
	`
	_, err = c.db.Exec(ctx,
		query,
		id.String(),
		req.Cash,
		req.Uzcard,
		req.Payme,
		req.Click,
		req.Humo,
		req.Apelsin,
		req.Visa,
		req.Currency,
		req.CurrencyCount,
		req.ExchangeSum,
		allPayment,
	)
	if err != nil {
		return nil, err
	}

	return &sell_service.PaymentPrimaryKey{Id: id.String()}, nil

}

func (c *PaymentRepo) GetByPKey(ctx context.Context, req *sell_service.PaymentPrimaryKey) (resp *sell_service.Payment, err error) {
	query := `
		SELECT
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			visa,
			currency,
			currency_count,
			exchange_sum,
			total_price,    
			created_at,
			updated_at
		FROM "payment"
		WHERE id = $1
	`

	var (
		id             sql.NullString
		cash           int64
		uzcard         int64
		payme          int64
		click          int64
		humo           int64
		apelsin        int64
		visa           int64
		currency       sql.NullString
		currency_count int64
		exchange_sum   int64
		total_price    int64
		createdAt      sql.NullString
		updatedAt      sql.NullString
	)

	err = c.db.QueryRow(ctx, query, req.Id).Scan(
		&id,
		&cash,
		&uzcard,
		&payme,
		&click,
		&humo,
		&apelsin,
		&visa,
		&currency,
		&currency_count,
		&exchange_sum,
		&total_price,
		&createdAt,
		&updatedAt,
	)

	if err != nil {
		return resp, err
	}

	resp = &sell_service.Payment{
		Id:            id.String,
		Cash:          cash,
		Uzcard:        uzcard,
		Payme:         payme,
		Click:         click,
		Humo:          humo,
		Apelsin:       apelsin,
		Visa:          visa,
		Currency:      currency.String,
		CurrencyCount: currency_count,
		ExchangeSum:   exchange_sum,
		TotalPrice:    total_price,
		CreatedAt:     createdAt.String,
		UpdatedAt:     updatedAt.String,
	}

	return
}

func (c *PaymentRepo) GetAll(ctx context.Context, req *sell_service.GetListPaymentRequest) (resp *sell_service.GetListPaymentResponse, err error) {

	resp = &sell_service.GetListPaymentResponse{}

	var (
		query  string
		limit  = ""
		offset = " OFFSET 0 "
		params = make(map[string]interface{})
		filter = " WHERE TRUE"
		sort   = " ORDER BY created_at DESC"
	)

	query = `
		SELECT
			COUNT(*) OVER(),
			id,
			cash,
			uzcard,
			payme,
			click,
			humo,
			apelsin,
			visa,
			currency,
			currency_count,
			exchange_sum,
			total_price,
			TO_CHAR(created_at, 'YYYY-MM-DD HH24:MI:SS'),
			TO_CHAR(updated_at, 'YYYY-MM-DD HH24:MI:SS')
		FROM "payment"
	`

	if req.GetLimit() > 0 {
		limit = " LIMIT :limit"
		params["limit"] = req.Limit
	}

	if req.GetOffset() > 0 {
		offset = " OFFSET :offset"
		params["offset"] = req.Offset
	}

	query += filter + sort + offset + limit

	query, args := helper.ReplaceQueryParams(query, params)
	rows, err := c.db.Query(ctx, query, args...)
	defer rows.Close()

	if err != nil {
		return resp, err
	}

	for rows.Next() {
		var (
			id             sql.NullString
			cash           int64
			uzcard         int64
			payme          int64
			click          int64
			humo           int64
			apelsin        int64
			visa           int64
			currency       sql.NullString
			currency_count int64
			exchange_sum   int64
			total_price    int64
			createdAt      sql.NullString
			updatedAt      sql.NullString
		)

		err := rows.Scan(
			&resp.Count,
			&id,
			&cash,
			&uzcard,
			&payme,
			&click,
			&humo,
			&apelsin,
			&visa,
			&currency,
			&currency_count,
			&exchange_sum,
			&total_price,
			&createdAt,
			&updatedAt,
		)

		if err != nil {
			return resp, err
		}

		resp.Payments = append(resp.Payments, &sell_service.Payment{
			Id:            id.String,
			Cash:          cash,
			Uzcard:        uzcard,
			Payme:         payme,
			Click:         click,
			Humo:          humo,
			Apelsin:       apelsin,
			Visa:          visa,
			Currency:      currency.String,
			CurrencyCount: currency_count,
			ExchangeSum:   exchange_sum,
			TotalPrice:    total_price,
			CreatedAt:     createdAt.String,
			UpdatedAt:     updatedAt.String,
		})
	}
	defer rows.Close()
	return
}

func (c *PaymentRepo) Update(ctx context.Context, req *sell_service.UpdatePayment) (rowsAffected int64, err error) {

	var (
		query      string
		params     map[string]interface{}
		allPayment int
	)
	allPayment = int(req.Cash) + int(req.Uzcard) + int(req.Payme) + int(req.Click) + int(req.Humo) + int(req.Apelsin) + int(req.Visa) + int(req.CurrencyCount)*int(req.ExchangeSum)

	query = `
			UPDATE
			    "payment"
			SET
				cash = :cash,
				uzcard = :uzcard,
				payme = :payme,
				click = :click,
				humo = :humo,
				apelsin = :apelsin,
				visa = :visa,
				currency = :currency,
				currency_count = :currency_count,
				exchange_sum = :exchange_sum,
				total_price = :total_price,
				updated_at = now()
			WHERE
				id = :id`
	params = map[string]interface{}{
		"id":             req.GetId(),
		"cash":           req.GetCash(),
		"uzcard":         req.GetUzcard(),
		"payme":          req.GetPayme(),
		"click":          req.GetClick(),
		"humo":           req.GetHumo(),
		"apelsin":        req.GetApelsin(),
		"visa":           req.GetVisa(),
		"currency":       req.GetCurrency(),
		"currency_count": req.GetCurrencyCount(),
		"exchange_sum":   req.GetExchangeSum(),
		"total_price":    allPayment,
	}

	query, args := helper.ReplaceQueryParams(query, params)

	result, err := c.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), nil
}

func (r *PaymentRepo) Patch(ctx context.Context, req *models.UpdatePatchRequest) (rowsAffected int64, err error) {

	var (
		set   = " SET "
		ind   = 0
		query string
	)

	if len(req.Fields) == 0 {
		err = errors.New("no updates provided")
		return
	}

	req.Fields["id"] = req.Id

	for key := range req.Fields {
		set += fmt.Sprintf(" %s = :%s ", key, key)
		if ind != len(req.Fields)-1 {
			set += ", "
		}
		ind++
	}

	query = `
		UPDATE
			"payment"
	` + set + ` , updated_at = now()
		WHERE
			id = :id
	`

	query, args := helper.ReplaceQueryParams(query, req.Fields)

	result, err := r.db.Exec(ctx, query, args...)
	if err != nil {
		return
	}

	return result.RowsAffected(), err

}

func (c *PaymentRepo) Delete(ctx context.Context, req *sell_service.PaymentPrimaryKey) (resp *emptypb.Empty, err error) {

	query := `DELETE FROM "payment" WHERE id = $1`

	_, err = c.db.Exec(ctx, query, req.Id)

	if err != nil {
		return nil, err
	}

	return
}
